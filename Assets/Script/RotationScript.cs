﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotationScript : MonoBehaviour
{
    //If you can't make it work, you can atleast make it look like it works for a presentation.
    public Animator anim;

    public void Start()
    {
        anim = GetComponent<Animator>();
    }


    public void RotateLeft()
    {
        AudioManager.Play("Selector");
        anim.SetTrigger("StepThree");
    }

    public void RotateRight()
    {
        AudioManager.Play("Selector");
        anim.SetTrigger("StepOne");
    }

    public void RotateUp()
    {
        AudioManager.Play("Selector");
        anim.SetTrigger("StepTwo");
    }
}
