﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class MenuScript : MonoBehaviour
{
    public Animator trasitionAnim;
    public Transform selector;

    public void Start()
    {
        GameObject lastSelection = EventSystem.current.currentSelectedGameObject;
    }

    GameObject lastSelection;
    public void SelectionChanged()
    {
        GameObject g = EventSystem.current.currentSelectedGameObject;
        selector.transform.position = g.transform.position;
        if (g != lastSelection && lastSelection != null)
        {
            AudioManager.Play("Selector");
        }
        lastSelection = g;
    }

    public void StartSceneOne()
    {
        StartCoroutine(LoadTrain());
    }

    public void Stop()
    {
        AudioManager.Play("Selector");
        Application.Quit();
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            Application.Quit();
        }
    }

  
    IEnumerator LoadTrain()
    {
        AudioManager.Play("Selector");
        trasitionAnim.SetBool("Start", true);
        yield return new WaitForSeconds(1.5f);
        SceneManager.LoadScene("Level");
    }
}